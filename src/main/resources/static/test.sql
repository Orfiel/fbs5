create table ACCOUNTS
(
  LOGIN VARCHAR(20) not null,
  USER_NAME VARCHAR(255) not null,
  USER_LAST_NAME VARCHAR(255) not null,
  ACTIVE BIT not null,
  ENCRYTED_PASSWORD VARCHAR(128) not null,
  USER_ROLE VARCHAR(20) not null,
  EMAIL VARCHAR(128) not null
) ;

alter table ACCOUNTS
  add primary key (LOGIN) ;


  insert into ACCOUNTS (LOGIN, USER_NAME, USER_LAST_NAME, ACTIVE, ENCRYTED_PASSWORD, USER_ROLE, EMAIL)
values ('user', 'Adam', 'Kowalski', 1,
'$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 'ROLE_USER', 'test@wp.pl');

insert into ACCOUNTS (LOGIN, USER_NAME, USER_LAST_NAME, ACTIVE, ENCRYTED_PASSWORD, USER_ROLE, EMAIL)
values ('admin', 'Ewa', 'Nowak', 1,
'$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 'ROLE_ADMIN', 'testowy2@onet.plk');